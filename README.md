# DevOps Automated Testing
This repository contains complete course companion and should be forked by participants.

## Agenda
### Day1
* Orientation: getting to know the environment
* Testing Basics: introduction to the complete testing cycle and basic principles
* Introduction to Standalone Selenium and Selenium IDE
* Practice: Select element
* Unit Testing
    * Javascript Basics
    * NPM Basics
    * Tools of Unit testing
        * Test Description: BDD Style Tests
        * Mocking: Mocha
        * Assert: Chai
* Demo: Web BMI Calculator unit Tests
* Practice: ???
* Web Service Testing Principles:
    * REST: Postman
    * SOAP: SoapUI
* Web Application Testing Principles:
    * Selenium Server
    * Cross Browser Testing
    * Remote selenium test execution
    * Reuse selenium session
    * Authentication
    * File Upload
* Practice: test basic authentication
* Automated Testing Best Practices:        
    * Organize test projects
    * REPL mode
    * Test Accounts and data management
    * Test reports
    * Docker
    * Page Objects
* Practice: rewrite login via test Page Objects


